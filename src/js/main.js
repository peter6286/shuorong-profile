document.addEventListener("DOMContentLoaded", function() {


    const header = document.querySelector("header");

    // Function to toggle the 'sticky' class and adjust size on scroll
    function toggleStickyAndSize() {
        const headerTop = header.offsetTop;
        const headerHeight = header.clientHeight;
        const scrollY = window.scrollY;


        if (scrollY > headerHeight) {
            header.classList.add("sticky");
            header.style.height = "70px"; // Adjust the larger size as needed
            header.style.transition = "height 0.3s ease"; // Add a smooth transition
            header.style.fontSize = "17px"; // Adjust the larger font size as needed
        } else {
            header.style.height = "150px"; // Adjust the original size as needed
            header.style.fontSize = "20px"; // Adjust the original font size as needed
        }
    }

    // Call the toggleStickyAndSize function on scroll
    window.addEventListener("scroll", toggleStickyAndSize);


    // Smooth scrolling for navigation links
    const navLinks = document.querySelectorAll("nav a");

    navLinks.forEach(link => {
        link.addEventListener("click", e => {
            e.preventDefault();
            const targetId = link.getAttribute("href").substring(1);
            const targetElement = document.getElementById(targetId);

            window.scrollTo({
                top: targetElement.offsetTop - header.clientHeight, // Adjust for the sticky header
                behavior: "smooth"
            });
        });
    });
});

document.addEventListener('DOMContentLoaded', function () {
    const slides = document.querySelectorAll('.slide');
    const prevBtn = document.querySelector('.prev-btn');
    const nextBtn = document.querySelector('.next-btn');

    let currentIndex = 0;

    function showSlide(index) {
        slides.forEach((slide, i) => {
            if (i === index) {
                slide.style.display = 'block';
            } else {
                slide.style.display = 'none';
            }
        });
    }

    function changeBackgroundColor(index) {
        const colors = ['#ff5733', 'red', 'skyblue']; // Define background colors
        document.querySelector('#carousel').style.backgroundColor = colors[index];
        showSlide(currentIndex);
    }

    function prevSlide() {
        currentIndex = (currentIndex - 1 + slides.length) % slides.length;
        showSlide(currentIndex);
        changeBackgroundColor(currentIndex);
    }

    function nextSlide() {
        currentIndex = (currentIndex + 1) % slides.length;
        showSlide(currentIndex);
        changeBackgroundColor(currentIndex);

    }

    showSlide(currentIndex); // Show the first slide initially
    changeBackgroundColor(currentIndex);

    prevBtn.addEventListener('click', prevSlide);
    nextBtn.addEventListener('click', nextSlide);
});


document.addEventListener('DOMContentLoaded', function () {
    const modalBtns = document.querySelectorAll('.portfolio-item img');
    const closeBtns = document.querySelectorAll('.close-btn');
    const modals = document.querySelectorAll('.modal');

    modalBtns.forEach(img => {
        img.addEventListener('click', () => {
            const modalId = img.getAttribute('data-modal');
            const modal = document.getElementById(modalId);
            modal.style.display = 'flex';
        });
    });

    closeBtns.forEach(btn => {
        btn.addEventListener('click', () => {
            const modalId = btn.getAttribute('data-modal');
            const modal = document.getElementById(modalId);
            modal.style.display = 'none';
        });
    });

    modals.forEach(modal => {
        modal.addEventListener('click', (e) => {
            if (e.target === modal) {
                modal.style.display = 'none';
            }
        });
    });
});

const sections = document.querySelectorAll('section');
const navLi = document.querySelectorAll('ul li');


// Listen for scroll events to update the highlighted menu item
window.addEventListener('scroll', () => {
    let current = '';
    sections.forEach((section, index) => {
        const sectionTop = section.offsetTop;
        const sectionHeight = section.clientHeight;

        if (pageYOffset >= sectionTop - sectionHeight / 2) {
            current = section.getAttribute('id');
        }

        // Additional condition to handle the last section
        if (index === sections.length - 1 &&
            window.innerHeight + window.pageYOffset >= document.body.offsetHeight) {
            current = section.getAttribute('id');
        }
    });

    navLi.forEach(li => {
        li.classList.remove('active');
        if (li.classList.contains(current)) {
            li.classList.add('active')
        }
    });


})





document.addEventListener('DOMContentLoaded', function() {
    let portfolioItems = document.querySelectorAll('.portfolio-item');

    portfolioItems.forEach(item => {
        item.addEventListener('click', function() {
            let modalId = this.getAttribute('data-modal');
            let modal = document.getElementById(modalId);

            if (modal) {
                modal.style.display = 'flex'; // Or any method to show your modal
            }
        });
    });
});





